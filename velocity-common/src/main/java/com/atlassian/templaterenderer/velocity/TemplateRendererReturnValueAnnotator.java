package com.atlassian.templaterenderer.velocity;

import com.atlassian.annotations.tenancy.TenancyScope;
import com.atlassian.annotations.tenancy.TenantAware;
import com.atlassian.velocity.htmlsafe.annotations.ReturnValueAnnotation;
import com.atlassian.velocity.htmlsafe.introspection.MethodAnnotator;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

public class TemplateRendererReturnValueAnnotator implements MethodAnnotator {
    private static final Logger log = LoggerFactory.getLogger(TemplateRendererReturnValueAnnotator.class);

    @TenantAware(value = TenancyScope.TENANTLESS)
    private final LoadingCache<Annotation, Boolean> annotationCache = CacheBuilder.newBuilder()
            .weakKeys()
            .build(new CacheLoader<Annotation, Boolean>() {
                @Override
                public Boolean load(final Annotation annotation) {
                    return (annotation.annotationType().isAnnotationPresent(ReturnValueAnnotation.class)
                            || annotation.annotationType().isAnnotationPresent(com.atlassian.templaterenderer.annotations.ReturnValueAnnotation.class));
                }
            });

    public Collection<Annotation> getAnnotationsForMethod(Method method) {
        Collection<Annotation> returnValueAnnotations = new HashSet<Annotation>();

        for (Annotation annotation : method.getAnnotations()) {
            if (annotationCache.getUnchecked(annotation)) {
                returnValueAnnotations.add(annotation);
            }
        }
        return Collections.unmodifiableCollection(returnValueAnnotations);
    }
}
